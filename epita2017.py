#!/usr/bin/env python3

from pypeul import IRC, Tags
import random
import requests
import requests.exceptions
from bs4 import BeautifulSoup
from bs4 import BeautifulStoneSoup
import threading
import time
import yaml
import codecs
import re

def load_conf():
    return yaml.load(codecs.open('conf.yml', 'r', 'utf-8'))

CONF = load_conf() 

def rdc(msg):
    return random.choice(CONF['msg'][msg])

def shortenurl(url):
    r = requests.get('http://ln-s.net/home/api.jsp', params={'url': url}).text
    return r.split()[1]

class EpitaForumAPI():
    def __init__(self):
        self.s = requests.Session()
   
    def connect(self):
        data = {
                  'action': 'do_login',
                  'url': 'http://epita2018.eu/index.php',
                  'username': CONF['forum']['pseudo'],
                  'password': CONF['forum']['password'],
                 }
        try:
            self.s.post('http://epita2018.eu/member.php', data=data)
        except requests.exceptions.RequestException:
           print("EPITA 2017 ne répond plus.")

    def get_unread_forums(self):
        response = self.s.get('http://epita2018.eu')
        soup = BeautifulSoup(response.text)
        unread = []
        for e in soup.find_all('img', src='images/twilight/on.gif'):
            if e.get('id'):
                unread.append(int(e.get('id').split('_')[2]))
        return unread
    
    def get_unread_threads(self, fid):
        response = self.s.get('http://epita2018.eu/forumdisplay.php?fid={}'.format(fid))
        soup = BeautifulSoup(response.text)
        unread = []
        table = soup.find('table', 'tborder')
        for row in table.find_all('tr'):
            thread = row.find('a', 'subject_new')
            if thread and thread.get('id'):
                pseudo = row.find('span', 'lastpost smalltext').find_all('a')[1].string
                tid = int(thread.get('id').split('_')[1])
                unread.append((tid, pseudo, thread.string))
        return unread

    def read_last(self, tid):
        response = self.s.get('http://epita2018.eu/showthread.php?tid={}&action=newpost'.format(tid))
        m = re.search(r'pid=([0-9]+)', response.url)
        if not m:
            return ''
        pid = int(m.group(1))
        soup = BeautifulSoup(response.text)
        post = soup.find('div', id=('pid_' + str(pid)))
        return post

    def clean_post(self, post):
        if not post:
            return ''
        for elt in post.find_all(['object', 'cite', 'blockquote']):
            if hasattr(elt, 'contents'):
                elt.decompose()
        text = post.get_text(strip=True)
        text = ' '.join(text.split('\n'))
        return text

class PeriodicalCall(threading.Thread):
    def __init__(self, delay, cls):
        super(PeriodicalCall, self).__init__()

        self._delay = delay
        self._cls = cls
        self.running = True

    def run(self):
        while self.running:
            self._cls.run()
            time.sleep(self._delay)

class Checker():
    def __init__(self, bot):
        self.bot = bot
        self.efapi = EpitaForumAPI()
        self.efapi.connect()
        self.http_error = False
    
    def run(self):
        if self.bot.ready:
            try:
                for f in self.efapi.get_unread_forums():
                    for tid, pseudo, thread in self.efapi.get_unread_threads(f):
                        print((tid, pseudo, thread))
                        link = shortenurl('http://epita2018.eu/showthread.php?tid={}&action=newpost'.format(tid))
                        post = self.efapi.read_last(tid)
                        post = self.efapi.clean_post(post)
                        self.bot.lastpost = post
                        if len(post) > CONF['forum']['maxextractsize']:
                            content = post[:(CONF['forum']['maxextractsize'])] + '…'
                        else:
                            content = post
                        msg = rdc('newpost').format(title=Tags.orange(thread),
                                nick=Tags.green(pseudo), url=link,
                                text=Tags.grey(content))
                        self.bot.message(bot.channel, msg)
                        self.http_error = False
                        time.sleep(2)
            except requests.exceptions.RequestException:
                if not self.http_error:
                    self.http_error = True
                    self.bot.message(self.bot.channel, rdc('http_error'))

class Bot(IRC):
    def __init__(self):
        IRC.__init__(self)
        self.channel = CONF['irc']['channel']
        self.channel_key = CONF['irc']['key']
        self.ready = False
        self.lastpost = None

    def on_ready(self):
        self.join(self.channel, self.channel_key)
        self.message(self.channel, rdc('hello'))
        self.ready = True

    def on_channel_message(self, umask, channel, msg):
        msg = Tags.strip(msg)
        admin = umask.host in CONF['admins']
        if channel != self.channel:
            return
        if msg[0] == '!':
            splitted = msg.split()
            command = splitted[0]
            args = splitted[1:]
            
            if command == '!lastpost':
                if self.lastpost:
                    post = self.lastpost
                    if len(post) > CONF['forum']['maxpostsize']:
                        post = post[:(CONF['forum']['maxpostsize'])] + '…'
                    self.message(self.channel, post)
                else:
                    self.message(self.channel, rdc('no_lastpost'))
            elif command == '!ping':
                self.message(self.channel, rdc('pong'))
            elif admin:
                if command == '!quit':
                    self.message(self.channel, rdc('quit'))
                    self.quit()

if __name__ == '__main__':
    bot = Bot()
    bot.connect(CONF['irc']['host'], CONF['irc']['port'])
    bot.ident(CONF['irc']['nick'])

    checker = Checker(bot)
    call = PeriodicalCall(CONF['forum']['checkdelay'], checker)
    call.daemon = True
    call.start()
   
    bot.run() 
